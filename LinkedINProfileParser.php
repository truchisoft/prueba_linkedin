<?php
// ----------------------------------------------------------------
// Creado por Juan José Sisti (c) 2014
// Importa perfiles de LinkedIN y crea un .csv con datos del perfil
// Tambien descarga la imagen.
// 
// Todo con el nombre del Perfil.
// ----------------------------------------------------------------

Function limpia($texto){
	return iconv('UTF-8', 'ISO-8859-1//TRANSLIT', $texto);
}

// Clase perfil, solo tiene un esqueleto básico de prueba, pero puede
// tener funcionalidades más complejas.
class Perfil
{
	public $imagen_url = '';
	public $nombre = '';
	public $apellido = '';
	public $puesto = '';
	public $empleador = '';
	public $herramientas = array();
}

// Obtenemos el directorio actual
$CurrDir = getcwd();
include "$CurrDir\simple_html_dom.php";

// Leemos el URL público del perfil que queremos.
$filePath = 'http://www.linkedin.com/in/juanjosesisti/es';
$html = file_get_html($filePath);

// LinkedIN tiene dividido en secciones llamadas "<span>" los datos
// del perfil, aprovechamos esto para obtener datos.
$info = $html->find('span[class]');
$valor = '';

//Creamos un nuevo perfil
$p = new Perfil();

// Ahora leemos todos los <span> y tomamos los que nos sirven.
// Es importante saber que si LinkedIN cambia algun valor,
// el proceso de importación no va a obtener todos los datos.
// Pero dependiendo de los cambios puede ser fácilmente parametrizable.
$flagTitle = false;
$flagOrg = false;
foreach($info as $element) {
       $valor = html_entity_decode(trim($element->plaintext));

		print "Clase: $element->class | Valor: $valor\n";

       switch ($element->class) {
       	case 'given-name':
       		$p->nombre = $valor;
       		break;
       	case 'family-name':
       		$p->apellido = $valor;
       		break;
       	case 'title':
       		if($flagTitle == false) {
	       		$p->puesto = $valor;
	       		$flagTitle = true;
       		}
       		break;
       	case 'org summary':
       		if($flagOrg == false) {
	       		$p->empleador = $valor;
	       		$flagOrg = true;
       		}
       		break;
       	case 'jellybean':
       		$p->herramientas[] = $valor;
       		break;
       }
}

// Ahora buscamos la foto de perfil, solo tomamos la de muestra para
// no azuzar demasiado y que no nos bloqueen el acceso.
$info2 = $html->find('div[id=profile-picture] img[class=photo]');
foreach($info2 as $elem2) { 
    $p->imagen_url = $elem2->src;
}


// Escribimos en el archivo .csv con datos, el nombre es igual al del perfil. 
$fp = fopen("$CurrDir\Datos " . limpia($p->nombre) . " " . $p->apellido . ".csv", 'w');
$linea = array('nombre','apellido', 'puesto', 'empleador', 'skills');
fputcsv($fp, $linea);
$linea = array($p->nombre, $p->apellido, $p->puesto, $p->empleador, implode("|", $p->herramientas));
fputcsv($fp, $linea);
fclose($fp);

// Descargamos la imagen, el nombre de la imagen es igual al del perfil.
$url = $p->imagen_url;
$img = $p->nombre . " " . $p->apellido . ".jpg";
file_put_contents(limpia($img), file_get_contents($url));
?>